# open, reading, readline, read-lines
# making file pointer
import fileinput

file = open("modes to open file")

# file “modes to open file” will open in "rt" mode as it is the default mode.
# But the best practice is to follow the syntax to avoid errors.

content = file.read()
print(content)
file.close()  # closing a file is must


# ####### READ A FILE IN PYTHON ########

# Two access modes are available reading (r).
#  We can also use the read(size) method

file = open("modes to open file", "r")
content = file.read(29)  # Here, you will get the first 28 characters of the file.
print(content)
file.close()

# Note: The default mode to read data is text mode.

file = open("modes to open file")
content = file.read()
print(content)
file.close()

# OR

file = open("modes to open file", "rt")
content = file.read()
print(content)
file.close()

#  If you want to read data in binary format, use ''rb", as it is not a default mode.

file = open("modes to open file", "rb")
content = file.read()
print(content)
file.close()


# ######## LINE BY LINE READING ###########

# 01.printing character by character

file = open("modes to open file", "rt")
content = file.read()
for line in content:
    print(line)
file.close()

# 02.default print of \n will open

file = open("modes to open file", "rt")
for lines in file:
    print(lines)
file.close()

# 03.putting 'end = ""' to remove default addition of \n

file = open("modes to open file", "rt")
for lines in file:
    print(lines, end="")
file.close()


# ######## READLINE METHOD ##########

# 01.You can use the readline() method to read individual lines of a file.

file = open("modes to open file", "rt")
lines = file.readline()
print(lines)  # only one line will printy
file.close()

# 02.printing multiple lines by readline method

file = open("modes to open file")
print(file.readline())
print(file.readline())
print(file.readline())
print(file.readline())
print(file.readline())
print(file.readline())

# 03.removing \n from last code by putting ' end="" '

file = open("modes to open file", "rt")
print(file.readline(), end="")
print(file.readline(), end="")
print(file.readline(), end="")
print(file.readline(), end="")
print(file.readline(), end="")


# ########### READ_LINES METHOD ##########

file = open("modes to open file")
print(file.readlines())
file.close()
