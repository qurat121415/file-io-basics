# ############# tell() function #############

# returns an integer giving the file pointer current position in the file
file = open("qurat.txt")
print(file.tell())
print(file.readline(), end="")
print(file.tell())
print(file.readline(), end="")
print(file.tell())

# what if we want to change the position of the file pointer.
# Here the concept of seek() function comes.


# ############# seek() function #############

file = open("qurat.txt")
file.seek(14)
print(file.readline())

# change the current file position to 14, and print the rest of the line

file.close()
