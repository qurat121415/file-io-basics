# ############ with block ############

# no close() function required
# "With block" creates a context manager that automatically closes a file after processing it.

with open("qurat.txt") as file:
    print(file.read(12))

# we can open multiple files in a single block by separating them using a comma.

with open("qurat.txt") as file, open("qurat.txt") as g:
    file.seek(12)
    print(file.readline())
    print(g.readline(), end="")
    print(g.readline(), end="")
    print(g.readline(), end="")
