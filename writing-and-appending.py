# different modes

# ############ "w" MODE ############

# NOTE: For a newly created file, it does no harm,
# but in case of already existing files, the previous data is lost as f.write() overrides it.

f = open("modes to open new file", "w")  # deletion of previous data from "modes to open file"
f.write("Qurat-Ul-Ain is a good girl")
f.close()

f = open("qurat.txt", "w")  # formation of new file
f.write("Qurat-Ul-Ain is a good girl")
f.close()


# ############ "a" MODE ############

# when we use "a" keyword, it adds more content at the end of the existing content.
file = open("modes to open file", "a")
file.write("appending words")
# the more you click on run, the more "appending words" will add at the end of existed file

# appending in new line
file = open("modes to open file", "a")
file.write("appending words \n")

# how many character have been written in file
file = open("modes to open file", "a")
characters = file.write("appending words \n")
print(characters)
file.close()


# ######### “r+” mode ############

# By opening a file in this mode, we can print the existing content on to the screen by printing f.read() function and
# adding or appending text to it using f.write() function.

# Handle read and write both
f = open("modes to open file", "r+")
print(f.read())
f.write("thank you")
